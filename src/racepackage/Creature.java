package racepackage;


/**
 * This is an abstract class that has the shared fields and methods
 * of the 3 different subclasses that use this super class.
 * 
 * 
 * @author Luke Van Hout
 * @date 2/13/22
 *
 */
public abstract class Creature {
	private String name; 
	private int maximumSpeed;
	private int currentPosition; // What index of the raceTrack array the creature is on
	private boolean flag; // Whether or not the animal has finished the race
	public char[] raceTrack;
	private char currentTerrain; 
	private char nextTerrain;
	
	/**
	 * Called by the subclasses, constructs the generic part of the creature. Sets the name, maximum
	 * speed, and racetrack according to the parameters. Sets the currentPosition to the start, 0. Sets
	 * the flag to false. And finally sets the currentTerrain to the terrain the creature is currently in
	 * and the nextTerrain to the next terrain it faces.
	 * 
	 * Preconditions: The creature has a name, max speed, and a corresponding racetrack.
	 * Postconditions: the current position starts at zero, the flag is false, and the current
	 * and next terrains are set based on the current position of the creature.
	 * 
	 * @param name Name of the creature
	 * @param maximumSpeed The maximum speed of the creature
	 * @param raceTrack The raceTrack the creature will be moving on
	 */
	public Creature(String name, int maximumSpeed, char[] raceTrack) {
		this.name = name;
		this.maximumSpeed = maximumSpeed;
		this.raceTrack = raceTrack;
		currentPosition = 0;
		flag = false;
		currentTerrain = raceTrack[getCurrentPosition()];
		nextTerrain = raceTrack[getCurrentPosition() + 1];
	}
	
	/**
	 * An abstract class because each of the sub-creatures moves in a different way
	 * 
	 * Postconditions: the creature gets movement points based off a random number between 1 and 
	 * their max movement points. based on the current and next terrain, they spend movement points 
	 * by moving around.
	 * 
	 * @param movement How many move points a creature has
	 */
	public abstract void move();
	
	/**
	 * Gets the creature's name
	 * 
	 * Preconditions: the creature has a name
	 * 
	 * @return The name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * Returns the maximum speed
	 * 
	 * Preconditions: the creature has a max speed
	 * 
	 * @return The maximum speed
	 */
	public int getMaxSpeed() {
		return maximumSpeed;
	}
	
	/**
	 * Returns the current position
	 * 
	 * Preconditions: the creature has a current position
	 * 
	 * @return the current position
	 */
	public int getCurrentPosition() {
		return currentPosition;
	}
	
	/**
	 * Sets flag to true, indicating the creature has finished the race
	 * 
	 * Postconditions: the flag is set from false to true
	 */
	public void setFlagTrue() {
		flag = true;
	}
	
	/**
	 * Returns the flag indicating whether or not the creature has finished the race
	 * 
	 * Preconditions: the creature has a flag
	 * 
	 * @return the flag
	 */
	public boolean getFlag() {
		return flag;
	}
	
	/**
	 * Returns the raceTrack (the array of characters)
	 * 
	 * Preconditions: the character is on a specific racetrack
	 * 
	 * @return the raceTrack
	 */
	public char[] getRaceTrack() {
		return raceTrack;
	}
	
	/**
	 * Returns the current terrain the animal is in
	 * 
	 * Preconditions: the creature has a current terrain
	 * 
	 * @return A character indicating what terrain it is in
	 */
	public char getCurrentTerrain() {
		return currentTerrain;
	}
	
	/**
	 * Returns the next terrain the animal will be in
	 * 
	 * Preconditions: the creature has another terrain in front of them
	 * 
	 * @return A character indicating what the next terrain will be
	 */
	public char getNextTerrain() {
		return nextTerrain;
	}
	
	/**
	 * Increases the creature's current position
	 * 
	 * Postconditions: the creatures current position is increased
	 */
	public void increaseCurrentPosition() {
		currentPosition++;
	}
	
	/**
	 * Increases the current postion and updates the currentTerrain and nextTerrain variables
	 * 
	 * Postconditions: the creature is moved forward and its current and next terrain are set
	 */
	public void increaseTerrainsAndPosition(){
		currentPosition++;
		currentTerrain = raceTrack[getCurrentPosition()];
		nextTerrain = raceTrack[getCurrentPosition() + 1];
	}
	
	/**
	 * Creates array of different names for creatures
	 * 
	 * Preconditions: the creature has a racer index
	 * Postconditions: the creature is given a name based on the index
	 * 
	 * @author Ellie Slater
	 * @param racerIndex Which racer is getting a name
	 * @return name The new name of the racer
	 */
	public String setRacerNames(int racerIndex) {
		String[] racerNames = {
			"Chad",
			"Ray",
			"Simba",
			"Singh",
			"Mamba",
			"Lulu",
			"Sly",
			"Sleek",
			"Chimp",
			"Zumba"
		};
		if(racerIndex > 9) {
			racerIndex -= 9;
		}
		name = racerNames[racerIndex];
		return name;
	}
	
	/**
	 * Gets the name of the racer based on the index
	 * 
	 * Preconditions: the racer has an index
	 * 
	 * @author Ellie Slater
	 * @param racerIndex Which racer name needs to be returned.
	 * @return name The name of the creature
	 */
	public String getRacerName(int racerIndex) {
		return name;
	}
}

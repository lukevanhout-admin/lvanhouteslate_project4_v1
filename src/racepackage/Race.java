package racepackage;

import java.util.Random;

/**
 * This class implements the methods shown in the RaceInterface
 * 
 * @author Ellie Slater
 * @date 2/18/22
 * CS 235
 */
public class Race implements RaceInterface {

	char[] racetrack;
	Creature[] racers;
	String[] winners;
	int length;
	int numRacers;
	
	/**
	 * Returns a random race track with different environments of a length provided
	 * 
	 * Preconditions: the race track exists
	 * 
	 * @return char[] The race track with different characters for different environments
	 */
	@Override
	public char[] getRacetrack() {
		// TODO Auto-generated method stub
		return racetrack;
	}	
	
	/**
	 * Gets a random number within a specific range
	 * 
	 * Preconditions: there are minimum and maximum values that the number can be within
	 * Postconditions: a random number is created
	 * 
	 * @param min The lowest number in the range
	 * @param max The highest number in the range
	 * @return randNum A random integer within the provided range
	 */
	public int getRandNum(int min, int max) {
		Random rand = new Random();
		int randNum = rand.nextInt(min, max+1);
		return randNum;
	}
	
	

	/**
	 * Gets the name of the racer based on their index in the array
	 * 
	 * Preconditions: the racer has an index
	 * Postconditions: the racer name is returned
	 * 
	 * @param racerIndex The index of the racer in the array of racers
	 * @return name The racer's name
	 */
	@Override
	public String getRacerName(int racerIndex) {
		// TODO Auto-generated method stub
		String name = racers[racerIndex].getRacerName(racerIndex);
		return name;
	}

	/**
	 * Takes racers index and use to get the position in the race track array
	 * 
	 * Preconditions: the racer has an index
	 * Postconditions: the position is returned
	 * 
	 * @param racerIndex The index of the racer
	 * @return 0 
	 */
	@Override
	public int getRacerPosition(int racerIndex) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	/**
	 * Takes the position of the winner to find out if they are a winner, meaning the index of their
	 * last turn in the same as the finish line
	 * 
	 * Preconditions: the racer has an index
	 * Postconditions: a boolean is returned based on if the racer is at the finish line
	 * 
	 * @param racerIndex The index of the racer in the racers array
	 * @return true If the racer is at the finish line
	 */
	@Override
	public boolean getRacerIsWinner(int racerIndex) {
		// TODO Auto-generated method stub
		int position = getRacerPosition(racerIndex);
		if(position == '|') {
			return true;
		}
		return false;
	}

	/**
	 * Creates the race display with a given race length and number of racers
	 * 
	 * Preconditions: a length and the number of racers are provided 
	 * Postconditions: a randomly generated race is generated with racers of random animal types
	 * 
	 * @param length The length of the race
	 * @param numRacers The number of racers participating in the race
	 */
	@Override
	public void createRace(int length, int numRacers) {
		// TODO Auto-generated method stub
		//this.numRacers = numRacers;
		
		racetrack = new char[length];
			
		for(int i = 0; i < length; i++) {
			int num = getRandNum(0, 3);
			
			if(i == length - 1) {
				racetrack[i] = '|'; //finish line
			} else if (num == 0) {
				racetrack[i] = '.'; //open plain
			} else if (num == 1) {
				racetrack[i] = '#'; //forest
			} else if (num == 2) {
				racetrack[i] = 'o'; //lake
			} else if (num == 3) {
				racetrack[i] = '~'; //desert
			} else {
				System.out.println("ERROR: making race track did not give randNum in range");
			}	
		}
		
		racers = new Creature[numRacers];
		
		for(int i = 0; i < numRacers; i++) {
			int rand = getRandNum(1, 3);
			if(rand == 1) {
				racers[i] = new Monkey("", racetrack);
				racers[i].setRacerNames(i);
			}
			else if(rand == 2) {
				racers[i] = new Ostrich("", racetrack);
				racers[i].setRacerNames(i);
			}
			else if (rand == 3) {
				racers[i] = new Turtle("", racetrack);
				racers[i].setRacerNames(i);
			}
		}
		advanceOneTurn();
	}
	

	/**
	 * Moves each player one turn until there are winners, then displays the final results 
	 * of the race
	 * 
	 * Postconditions: the race is fully executed with the results and winners displayed
	 */
	@Override
	public void advanceOneTurn() {
		// TODO Auto-generated method stub
		//for each creature of type Creature in creatures
		//creature.move()
		boolean isWinner = false;
		while(isWinner == false) {
			for(int i = 0; i < racers.length; i++) {
				//int rand = getRandNum();
				racers[i].move();
				if(racers[i].getCurrentPosition() >= racetrack.length - 1) {
					isWinner = true;
				}
			}
		}
		if(isWinner == true) {
			for(int i = 0; i < racers.length; i++) {
				String str = "";
				if(racers[i] instanceof Monkey) {
					str = "Monkey (" + getRacerName(i) + ") " + i +":" ;
				}
				else if(racers[i] instanceof Ostrich) {
					str = "Ostrich (" + getRacerName(i) + ") " + i +":" ;
				}
				else if(racers[i] instanceof Turtle){
					str = "Turtle (" + getRacerName(i) + ") " + i +":" ;
				}
				System.out.print(str);
				for(int j = 0; j < racetrack.length; j++) {
					if(racers[i].getCurrentPosition() == j) {
						System.out.print("<" + racetrack[j] + ">");
					}
					else {
						System.out.print(" " + racetrack[j] + " ");
					}
				}
				System.out.print("\n");
			}
			
			System.out.println("\nWinner(s)");
			for(int i = 0; i < racers.length; i++) {
				if(racers[i].getCurrentPosition() == racetrack.length - 1) {
					System.out.println(racers[i].getRacerName(i));
				}
			}
		}
	}
	
	/**
	 * The main method of the program
	 * 
	 * Postconditions: a new race is created that has a length of 20 and 10 racers
	 * 
	 * @param args The arguments of the main method
	 */
	public static void main(String[] args) {
		Race race = new Race();
		race.createRace(20, 10);
	}

}

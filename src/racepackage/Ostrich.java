package racepackage;

import java.util.Random;

/**
 * A turtle object that is a subclass of creature
 * 
 * @author Luke Van Hout
 * @date 2/13/22
 * 
 */
public class Ostrich extends Creature{

	private static int MAX_SPEED = 5; // Maximum number of movement points

	/**
	 * Creates an Ostrich
	 * 
	 * Preconditions: the ostrich has a name and specific race track
	 * Postconditions: an ostrich is created
	 * 
	 * @param name The name of the ostrich
	 * @param raceTrack The race track the ostrich is on
	 */
	public Ostrich(String name, char[] raceTrack) {
		super(name, MAX_SPEED, raceTrack);

	}

	/**
	 * Constructs a ostrich with a name and an array of characters going to the super class
	 * The static field MAX_SPEED is also fed to the super class
	 * 
	 * Postconditions: the ostrich moves according to movement points and terrains
	 * 
	 * @param name The name of the ostrich
	 * @param raceTrack Array of characters
	 */
	@Override
	public void move() {
		Random rand = new Random();
		int randNum = rand.nextInt(0, MAX_SPEED);
		int movement = randNum; // Set the amount of movement the Monkey has for this turn
		boolean canMove = true;

		if(getCurrentTerrain() == '.' || getCurrentTerrain() == '~') {
			movement++; // If the ostrich starts in a desert or an open plains it gains one movement point
		}
		while(canMove) {
			if(getCurrentTerrain() == getNextTerrain()) {
				increaseTerrainsAndPosition(); // Free to move through the same terrain it is already in
			}
			else if(getNextTerrain() == '#') {
				if(movement >= 3) {
					increaseTerrainsAndPosition();
					movement -= 3; // 3 movement points to move into a forest
				}
				else {
					canMove = false;
				}
			}
			else if(getNextTerrain() == 'o') {
				if(movement >= 2) {
					increaseTerrainsAndPosition();
					movement -= 2; // 2 movement points to move into a lake
				}
				else {
					canMove = false;
				}
			}
			else if(getNextTerrain() == '|') {
				if(movement >= 1) {
					increaseCurrentPosition();
					setFlagTrue(); // Indicates the ostrich has finished the game
					movement --; // 1 movement point to cross the finish line
				}
				else {
					canMove = false;
				}
			}
			else {
				if(movement >= 1) {
					increaseTerrainsAndPosition();
					movement --; // Costs 1 movement point for all non-specified terrain
				}
				else {
					canMove = false;
				}
			}
		}
	}
}


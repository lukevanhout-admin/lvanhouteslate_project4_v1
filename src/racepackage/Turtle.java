package racepackage;

import java.util.Random;

/**
 * A turtle object that is a subclass of creature
 * 
 * @author Luke Van Hout
 * @date 2/13/22
 * 
 */
public class Turtle extends Creature{
	
	private static int MAX_SPEED = 2; // Maximum number of movement points

	/**
	 * Constructs a turtle with a name and an array of characters going to the super class
	 * The static field MAX_SPEED is also fed to the super class
	 * 
	 * Preconditions: the turtle has a name and racetrack
	 * Postconditions: a turtle is created
	 * 
	 * @param name The name of the turtle
	 * @param raceTrack Array of characters
	 */
	public Turtle(String name, char[] raceTrack) {
		super(name, MAX_SPEED, raceTrack);
	}

	/**
	 * Moves the turtle forward in the race according to the amount of movement it has
	 * and how it can move through different terrains
	 * 
	 * Postconditions: the turtle is moved according to movement points and terrains
	 */
	@Override
	public void move() {
		Random rand = new Random();
		int randNum = rand.nextInt(0, MAX_SPEED);
		int movement = randNum; // Set the amount of movement the Monkey has for this turn
		boolean canMove = true;
		
		while(canMove) {
			if(getCurrentTerrain() == getNextTerrain() || getNextTerrain() == 'o') {
				increaseTerrainsAndPosition(); // No movement cost if the current and next terrain are the same
				   							   // or if the next terrain is a lake
			}
			else if(getNextTerrain() == '|') {
				if(movement >= 1) {
					increaseCurrentPosition();
					setFlagTrue(); // Indicates the turtle has finished the race
					movement --; // Costs one movement point for the turtle to finish the race
				}
				else {
					canMove = false;
				}
			}
			else {
				if(movement >= 1) {
					increaseTerrainsAndPosition();
					movement --; // Costs one movement point for all non-specified terrains
				}
				else {
					canMove = false;
				}
			}
		}
	}
}

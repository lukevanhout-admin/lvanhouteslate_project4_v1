package racepackage;

import java.util.Random;

/**
 * A monkey object that is a subclass of creature
 * 
 * @author Luke Van Hout
 * @date 2/13/22
 * 
 */
public class Monkey extends Creature{
	private static int MAX_SPEED = 4; // Maximum number of movement points
	
	/**
	 * Constructs a monkey with a name and an array of characters going to the super class
	 * The static field MAX_SPEED is also fed to the super class
	 * 
	 * Preconditions: the creature has a name and a specific racetrack
	 * Postconditions: a monkey is created
	 * 
	 * @param name Name of the Monkey
	 * @param raceTrack Array of characters
	 */
	public Monkey(String name, char[] raceTrack) {
		super(name, MAX_SPEED, raceTrack);
		
	}

	/**
	 * Moves the monkey forward in the race according to the amount of movement it has
	 * and how it can move through different terrains
	 * 
	 * Postconditions: the monkey moves based on movement points and conditions on the 
	 * next terrain
	 */
	@Override
	public void move() {
		Random rand = new Random();
		int randNum = rand.nextInt(0, MAX_SPEED);
		int movement = randNum; // Set the amount of movement the Monkey has for this turn
		boolean canMove = true;
		
		while(canMove) {
			if(getCurrentTerrain() == getNextTerrain() || getNextTerrain() == '#') { 
				increaseTerrainsAndPosition(); // No movement cost if the current and next terrain are the same
											   // or if the next terrain is a forest
			}
			else if(getNextTerrain() == '~') {
				if(movement >= 2) {
					increaseTerrainsAndPosition();
					movement -= 2; // Costs two movement points to move through the desert terrain
				}
				else {
					canMove = false;
				}
			}
			else if(getNextTerrain() == 'o') {
				increaseTerrainsAndPosition();
				movement = 0; // Entering a lake gets rid of all remaining movement points
				canMove = false;
			}
			else if(getNextTerrain() == '|') {
				if(movement >= 1) {
					increaseCurrentPosition();
					setFlagTrue(); // Indicates the monkey has finished the race
					movement --; // Costs one movement to cross the finish line
				}
				else {
					canMove = false;
				}
			}
			else {
				if(movement >= 1) {
					increaseTerrainsAndPosition();
					movement --; // All non-specified terrains cost one movement point
				}
				else {
					canMove = false;
				}
			}
		}
	}
}